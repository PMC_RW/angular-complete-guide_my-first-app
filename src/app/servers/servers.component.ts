import { Component } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  // can be template: `<...>` instead
  styleUrls: ['./servers.component.css'],
  // can be styles: "h1: {color: blue}" instead
})
export class ServersComponent {
  allowNewServer: boolean = false;
  newName: string = '';
  serverCreationStatus: string = '';
  serverCount: number = 0;
  servers: object[] = [
    { id: '1', name: 'Heli' },
    { id: '2', name: 'Bus' },
  ];

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
  }

  onChangeServerName(event: any) {
    this.newName = event.target.value;
  }

  onCreateServer() {
    this.servers.push({ id: this.servers.length + 1, name: this.newName });
    this.serverCount++;
    this.serverCreationStatus = this.newName + ' was created';
  }
}
