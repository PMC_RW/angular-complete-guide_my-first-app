import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'my-first-app';

  showDetails = false;
  count = 1;
  toggleLog = [];

  onToggleDetails() {
    this.showDetails = !this.showDetails;
    this.toggleLog.push({
      count: this.count++,
      time: new Date().toLocaleTimeString(),
    });
  }
}
