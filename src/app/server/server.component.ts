import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styles: [
    `
      .borderGreen {
        border: 1px solid green;
      }
    `,
  ],
})
export class ServerComponent {
  @Input() server;
  isOnline: boolean;

  constructor() {
    this.isOnline = Math.random() > 0.5 ? true : false;
  }

  getServerStatus(): string {
    if (this.isOnline) {
      return 'Online';
    } else {
      return 'Offline';
    }
  }

  getColor(): string {
    return this.isOnline ? 'green' : 'red';
  }
}
